package com.example.aroundyou.venue;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "venue_table")
public class Venue {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private VenueLocation location;
    private VenueCategory[] categories;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VenueLocation getLocation() {
        return location;
    }

    public void setLocation(VenueLocation location) {
        this.location = location;
    }

    public VenueCategory[] getCategories() {
        return categories;
    }

    public void setCategories(VenueCategory[] categories) {
        this.categories = categories;
    }
}
