package com.example.aroundyou.venue;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "venue_location_table")
public class VenueLocation {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String address;
    private long lat;
    private long lng;
    private String postalCode;
    private String city;
    private String[] formattedAddress ;

}
