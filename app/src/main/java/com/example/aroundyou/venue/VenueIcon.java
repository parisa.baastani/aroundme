package com.example.aroundyou.venue;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "venue_icon_table")
public class VenueIcon {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String prefix;
    private String suffix;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
