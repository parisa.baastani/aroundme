package com.example.aroundyou.venue;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "venue_category_table")
public class VenueCategory {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private VenueIcon icon;
    private boolean primary;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VenueIcon getIcon() {
        return icon;
    }

    public void setIcon(VenueIcon icon) {
        this.icon = icon;
    }
    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }
}
