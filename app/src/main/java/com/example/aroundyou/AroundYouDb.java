package com.example.aroundyou;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.aroundyou.venue.Venue;

@Database(entities = {Venue.class}, version = 1)
abstract class AroundYouDb extends RoomDatabase {
    private static AroundYouDb instance;
    public abstract VenueDao venueDao();
    public synchronized AroundYouDb getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context,AroundYouDb.class,"around_you_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
