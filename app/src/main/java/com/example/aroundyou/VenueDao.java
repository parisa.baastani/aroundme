package com.example.aroundyou;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.aroundyou.venue.Venue;

import java.util.List;

@Dao
public interface VenueDao {

    @Insert
    void insert(Venue venue);

    @Update
    void update(Venue venue);

    @Delete
    void delete(Venue venue);

    @Query("DELETE FROM venue_table")
    void deleteAll();

    @Query("SELECT * FROM venue_table")
    LiveData<List<Venue>> getVenueList();
}
